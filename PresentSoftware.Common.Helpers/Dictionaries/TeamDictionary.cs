﻿using System.Collections.Generic;

namespace PresentSoftware.Common.Helpers.Dictionaries
{
    public static class TeamDictionary
    {
        
        #region Fields
        private static Dictionary<string, int> _ArizonaRedBirdsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _AtlantaFowlsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _BaltimoreVulturesTeamAttributeDictionary = null;
        private static Dictionary<string, int> _BuffalosTeamAttributeDictionary = null;
        private static Dictionary<string, int> _CarolinaCatsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _ChicagoGrizzliesTeamAttributeDictionary = null;
        private static Dictionary<string, int> _CincinnatiTigersTeamAttributeDictionary = null;
        private static Dictionary<string, int> _ClevelandBluesTeamAttributeDictionary = null;
        private static Dictionary<string, int> _DallasAmericansTeamAttributeDictionary = null;
        private static Dictionary<string, int> _DenverMustangsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _DetroitCheetahsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _GreenBayBackersTeamAttributeDictionary = null;
        private static Dictionary<string, int> _HoustonAstronautsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _IndianapolisThoroughbredsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _JacksonvilleLynxTeamAttributeDictionary = null;
        private static Dictionary<string, int> _KansasCityNativesTeamAttributeDictionary = null;
        private static Dictionary<string, int> _MiamiSharksTeamAttributeDictionary = null;
        private static Dictionary<string, int> _MinnesotaCrusadersTeamAttributeDictionary = null;
        private static Dictionary<string, int> _NewEnglandTeaBagsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _NewJerseyMigsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _NewOrleansCajunsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _OaklandPillagersTeamAttributeDictionary = null;
        private static Dictionary<string, int> _PhiledelphiaTurkeysTeamAttributeDictionary = null;
        private static Dictionary<string, int> _PittsburghMinersTeamAttributeDictionary = null;
        private static Dictionary<string, int> _SanDiegoPowerTeamAttributeDictionary = null;
        private static Dictionary<string, int> _Sanfrancisco1999ersTeamAttributeDictionary = null;
        private static Dictionary<string, int> _SeattleSeagullsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _StLouisGoatsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _TampaBayPiratesTeamAttributeDictionary = null;
        private static Dictionary<string, int> _TennesseMountainsTeamAttributeDictionary = null;
        private static Dictionary<string, int> _WashingtonMemorialsTeamAttributeDictionary = null;
        public static string NewYorkTitans = "New York Titans";
        public static string PhiledelphiaTurkeys = "Philedelphia Turkeys";
        public static string WashingtonMemorials = "Washington Memorials";
        public static string DallasAmericans = "Dallas Americans";
        public static string NewOrleansCajuns = "New Orleans Cajuns";
        public static string AtlantaFowls = "Atlanta Fowls";
        public static string CarolinaCats = "Carolina Cats";
        public static string TampaBayPirates = "Tampa Bay Pirates";
        public static string ChicagoGrizzlies = "Chicago Grizzlies";
        public static string GreenBayBackers = "Green Bay Backers";
        public static string DetroitCheetahs = "Detroit Cheetahs";
        public static string MinnesotaCrusaders = "Minnesota Crusaders";
        public static string Sanfrancisco1999ers = "Sanfrancisco 1999ers";
        public static string StLouisGoats = "St Louis Goats";
        public static string SeattleSeagulls = "Seattle Seagulls";
        public static string ArizonaRedBirds = "Arizona Red Birds";
        public static string NewJerseyMigs = "New Jersey Migs";
        public static string NewEnglandTeaBags = "New England Tea Bags";
        public static string MiamiSharks = "Miami Sharks";
        public static string Buffalos = "Buffalos";
        public static string TennesseMountains = "Tennesse Mountains";
        public static string IndianapolisThoroughbreds = "Indianapolis Thoroughbreds";
        public static string HoustonAstronauts = "Houston Astronauts";
        public static string JacksonvilleLynx = "Jacksonville Lynx";
        public static string PittsburghMiners = "Pittsburgh Miners";
        public static string CincinnatiTigers = "Cincinnati Tigers";
        public static string BaltimoreVultures = "Baltimore Vultures";
        public static string ClevelandBlues = "Cleveland Blues";
        public static string KansasCityNatives = "Kansas City Natives";
        public static string OaklandPillagers = "Oakland Pillagers";
        public static string SanDiegoPower = "San diego Power";
        public static string DenverMustangs = "Denver Mustangs";

        public static string FanLoyalty = "FanLoyalty";
        public static string MegaBowlsWon = "MegaBowlsWon";
        public static string HallOfFamers = "HallOfFamers";
        public static string Prestige = "Prestige";

        private static List<string> _TeamList;

        private static Dictionary<string, int> _NewYorkTitansTeamAttributeDictionary;
        private static Dictionary<string, Dictionary<string, int>> _MasterTeamAttributeDictionary;

        #endregion

        #region Properties        
        public static Dictionary<string, Dictionary<string, int>> MasterTeamAttributeDictionary
        {
            get
            {
                if (_MasterTeamAttributeDictionary == null)
                {
                    _MasterTeamAttributeDictionary = new Dictionary<string, Dictionary<string, int>>();
                    _MasterTeamAttributeDictionary.Add(NewYorkTitans, NewYorkTitansTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(PhiledelphiaTurkeys, PhiledelphiaTurkeysTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(WashingtonMemorials, WashingtonMemorialsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(DallasAmericans, DallasAmericansTeamAttributeDictionary);

                    _MasterTeamAttributeDictionary.Add(NewOrleansCajuns, NewOrleansCajunsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(AtlantaFowls, AtlantaFowlsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(CarolinaCats, CarolinaCatsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(TampaBayPirates, TampaBayPiratesTeamAttributeDictionary);

                    _MasterTeamAttributeDictionary.Add(ChicagoGrizzlies, ChicagoGrizzliesTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(GreenBayBackers, GreenBayBackersTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(DetroitCheetahs, DetroitCheetahsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(MinnesotaCrusaders, MinnesotaCrusadersTeamAttributeDictionary);

                    _MasterTeamAttributeDictionary.Add(Sanfrancisco1999ers, Sanfrancisco1999ersTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(StLouisGoats, StLouisGoatsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(SeattleSeagulls, SeattleSeagullsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(ArizonaRedBirds, ArizonaRedBirdsTeamAttributeDictionary);

                    _MasterTeamAttributeDictionary.Add(NewJerseyMigs, NewJerseyMigsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(NewEnglandTeaBags, NewEnglandTeaBagsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(MiamiSharks, MiamiSharksTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(Buffalos, BuffalosTeamAttributeDictionary);

                    _MasterTeamAttributeDictionary.Add(TennesseMountains, TennesseMountainsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(IndianapolisThoroughbreds, IndianapolisThoroughbredsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(HoustonAstronauts, HoustonAstronautsTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(JacksonvilleLynx, JacksonvilleLynxTeamAttributeDictionary);

                    _MasterTeamAttributeDictionary.Add(PittsburghMiners, PittsburghMinersTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(CincinnatiTigers, CincinnatiTigersTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(BaltimoreVultures, BaltimoreVulturesTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(ClevelandBlues, ClevelandBluesTeamAttributeDictionary);

                    _MasterTeamAttributeDictionary.Add(KansasCityNatives, KansasCityNativesTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(OaklandPillagers, OaklandPillagersTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(SanDiegoPower, SanDiegoPowerTeamAttributeDictionary);
                    _MasterTeamAttributeDictionary.Add(DenverMustangs, DenverMustangsTeamAttributeDictionary);
                }
                return _MasterTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> NewYorkTitansTeamAttributeDictionary
        {
            get
            {
                if (_NewYorkTitansTeamAttributeDictionary == null)
                {
                    _NewYorkTitansTeamAttributeDictionary = new Dictionary<string, int>();
                    _NewYorkTitansTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _NewYorkTitansTeamAttributeDictionary.Add(MegaBowlsWon, 4);
                    _NewYorkTitansTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _NewYorkTitansTeamAttributeDictionary.Add(Prestige, 9);
                }
                return _NewYorkTitansTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> PhiledelphiaTurkeysTeamAttributeDictionary
        {
            get
            {
                if (_PhiledelphiaTurkeysTeamAttributeDictionary == null)
                {
                    _PhiledelphiaTurkeysTeamAttributeDictionary = new Dictionary<string, int>();
                    _PhiledelphiaTurkeysTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _PhiledelphiaTurkeysTeamAttributeDictionary.Add(MegaBowlsWon, 0);
                    _PhiledelphiaTurkeysTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _PhiledelphiaTurkeysTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _PhiledelphiaTurkeysTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> WashingtonMemorialsTeamAttributeDictionary
        {
            get
            {
                if (_WashingtonMemorialsTeamAttributeDictionary == null)
                {
                    _WashingtonMemorialsTeamAttributeDictionary = new Dictionary<string, int>();
                    _WashingtonMemorialsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _WashingtonMemorialsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _WashingtonMemorialsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _WashingtonMemorialsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _WashingtonMemorialsTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> DallasAmericansTeamAttributeDictionary
        {
            get
            {
                if (_DallasAmericansTeamAttributeDictionary == null)
                {
                    _DallasAmericansTeamAttributeDictionary = new Dictionary<string, int>();
                    _DallasAmericansTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _DallasAmericansTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _DallasAmericansTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _DallasAmericansTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _DallasAmericansTeamAttributeDictionary;
            }
        }
        public static Dictionary<string, int> NewOrleansCajunsTeamAttributeDictionary
        {
            get
            {
                if (_NewOrleansCajunsTeamAttributeDictionary == null)
                {
                    _NewOrleansCajunsTeamAttributeDictionary = new Dictionary<string, int>();
                    _NewOrleansCajunsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _NewOrleansCajunsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _NewOrleansCajunsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _NewOrleansCajunsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _NewOrleansCajunsTeamAttributeDictionary;
            }
        }
        public static Dictionary<string, int> AtlantaFowlsTeamAttributeDictionary
        {
            get
            {
                if (_AtlantaFowlsTeamAttributeDictionary == null)
                {
                    _AtlantaFowlsTeamAttributeDictionary = new Dictionary<string, int>();
                    _AtlantaFowlsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _AtlantaFowlsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _AtlantaFowlsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _AtlantaFowlsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _AtlantaFowlsTeamAttributeDictionary;
            }
        }
        public static Dictionary<string, int> CarolinaCatsTeamAttributeDictionary
        {
            get
            {
                if (_CarolinaCatsTeamAttributeDictionary == null)
                {
                    _CarolinaCatsTeamAttributeDictionary = new Dictionary<string, int>();
                    _CarolinaCatsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _CarolinaCatsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _CarolinaCatsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _CarolinaCatsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _CarolinaCatsTeamAttributeDictionary;
            }
        }
        public static Dictionary<string, int> TampaBayPiratesTeamAttributeDictionary
        {
            get
            {
                if (_TampaBayPiratesTeamAttributeDictionary == null)
                {
                    _TampaBayPiratesTeamAttributeDictionary = new Dictionary<string, int>();
                    _TampaBayPiratesTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _TampaBayPiratesTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _TampaBayPiratesTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _TampaBayPiratesTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _TampaBayPiratesTeamAttributeDictionary;
            }
        }
        public static Dictionary<string, int> ChicagoGrizzliesTeamAttributeDictionary
        {
            get
            {
                if (_ChicagoGrizzliesTeamAttributeDictionary == null)
                {
                    _ChicagoGrizzliesTeamAttributeDictionary = new Dictionary<string, int>();
                    _ChicagoGrizzliesTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _ChicagoGrizzliesTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _ChicagoGrizzliesTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _ChicagoGrizzliesTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _ChicagoGrizzliesTeamAttributeDictionary;
            }
        }
        public static Dictionary<string, int> GreenBayBackersTeamAttributeDictionary
        {
            get
            {
                if (_GreenBayBackersTeamAttributeDictionary == null)
                {
                    _GreenBayBackersTeamAttributeDictionary = new Dictionary<string, int>();
                    _GreenBayBackersTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _GreenBayBackersTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _GreenBayBackersTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _GreenBayBackersTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _GreenBayBackersTeamAttributeDictionary;
            }
        }
        public static Dictionary<string, int> DetroitCheetahsTeamAttributeDictionary
        {
            get
            {
                if (_DetroitCheetahsTeamAttributeDictionary == null)
                {
                    _DetroitCheetahsTeamAttributeDictionary = new Dictionary<string, int>();
                    _DetroitCheetahsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _DetroitCheetahsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _DetroitCheetahsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _DetroitCheetahsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _DetroitCheetahsTeamAttributeDictionary;
            }
        }
        public static Dictionary<string, int> MinnesotaCrusadersTeamAttributeDictionary
        {
            get
            {
                if (_MinnesotaCrusadersTeamAttributeDictionary == null)
                {
                    _MinnesotaCrusadersTeamAttributeDictionary = new Dictionary<string, int>();
                    _MinnesotaCrusadersTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _MinnesotaCrusadersTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _MinnesotaCrusadersTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _MinnesotaCrusadersTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _MinnesotaCrusadersTeamAttributeDictionary;
            }
        }
        public static Dictionary<string, int> Sanfrancisco1999ersTeamAttributeDictionary
        {
            get
            {
                if (_Sanfrancisco1999ersTeamAttributeDictionary == null)
                {
                    _Sanfrancisco1999ersTeamAttributeDictionary = new Dictionary<string, int>();
                    _Sanfrancisco1999ersTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _Sanfrancisco1999ersTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _Sanfrancisco1999ersTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _Sanfrancisco1999ersTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _Sanfrancisco1999ersTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> StLouisGoatsTeamAttributeDictionary
        {
            get
            {
                if (_StLouisGoatsTeamAttributeDictionary == null)
                {
                    _StLouisGoatsTeamAttributeDictionary = new Dictionary<string, int>();
                    _StLouisGoatsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _StLouisGoatsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _StLouisGoatsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _StLouisGoatsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _StLouisGoatsTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> SeattleSeagullsTeamAttributeDictionary
        {
            get
            {
                if (_SeattleSeagullsTeamAttributeDictionary == null)
                {
                    _SeattleSeagullsTeamAttributeDictionary = new Dictionary<string, int>();
                    _SeattleSeagullsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _SeattleSeagullsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _SeattleSeagullsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _SeattleSeagullsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _SeattleSeagullsTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> ArizonaRedBirdsTeamAttributeDictionary
        {
            get
            {
                if (_ArizonaRedBirdsTeamAttributeDictionary == null)
                {
                    _ArizonaRedBirdsTeamAttributeDictionary = new Dictionary<string, int>();
                    _ArizonaRedBirdsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _ArizonaRedBirdsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _ArizonaRedBirdsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _ArizonaRedBirdsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _ArizonaRedBirdsTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> NewJerseyMigsTeamAttributeDictionary
        {
            get
            {
                if (_NewJerseyMigsTeamAttributeDictionary == null)
                {
                    _NewJerseyMigsTeamAttributeDictionary = new Dictionary<string, int>();
                    _NewJerseyMigsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _NewJerseyMigsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _NewJerseyMigsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _NewJerseyMigsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _NewJerseyMigsTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> NewEnglandTeaBagsTeamAttributeDictionary
        {
            get
            {
                if (_NewEnglandTeaBagsTeamAttributeDictionary == null)
                {
                    _NewEnglandTeaBagsTeamAttributeDictionary = new Dictionary<string, int>();
                    _NewEnglandTeaBagsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _NewEnglandTeaBagsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _NewEnglandTeaBagsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _NewEnglandTeaBagsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _NewEnglandTeaBagsTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> MiamiSharksTeamAttributeDictionary
        {
            get
            {
                if (_MiamiSharksTeamAttributeDictionary == null)
                {
                    _MiamiSharksTeamAttributeDictionary = new Dictionary<string, int>();
                    _MiamiSharksTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _MiamiSharksTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _MiamiSharksTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _MiamiSharksTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _MiamiSharksTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> BuffalosTeamAttributeDictionary
        {
            get
            {
                if (_BuffalosTeamAttributeDictionary == null)
                {
                    _BuffalosTeamAttributeDictionary = new Dictionary<string, int>();
                    _BuffalosTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _BuffalosTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _BuffalosTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _BuffalosTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _BuffalosTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> TennesseMountainsTeamAttributeDictionary
        {
            get
            {
                if (_TennesseMountainsTeamAttributeDictionary == null)
                {
                    _TennesseMountainsTeamAttributeDictionary = new Dictionary<string, int>();
                    _TennesseMountainsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _TennesseMountainsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _TennesseMountainsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _TennesseMountainsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _TennesseMountainsTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> IndianapolisThoroughbredsTeamAttributeDictionary
        {
            get
            {
                if (_IndianapolisThoroughbredsTeamAttributeDictionary == null)
                {
                    _IndianapolisThoroughbredsTeamAttributeDictionary = new Dictionary<string, int>();
                    _IndianapolisThoroughbredsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _IndianapolisThoroughbredsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _IndianapolisThoroughbredsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _IndianapolisThoroughbredsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _IndianapolisThoroughbredsTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> HoustonAstronautsTeamAttributeDictionary
        {
            get
            {
                if (_HoustonAstronautsTeamAttributeDictionary == null)
                {
                    _HoustonAstronautsTeamAttributeDictionary = new Dictionary<string, int>();
                    _HoustonAstronautsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _HoustonAstronautsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _HoustonAstronautsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _HoustonAstronautsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _HoustonAstronautsTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> JacksonvilleLynxTeamAttributeDictionary
        {
            get
            {
                if (_JacksonvilleLynxTeamAttributeDictionary == null)
                {
                    _JacksonvilleLynxTeamAttributeDictionary = new Dictionary<string, int>();
                    _JacksonvilleLynxTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _JacksonvilleLynxTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _JacksonvilleLynxTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _JacksonvilleLynxTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _JacksonvilleLynxTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> PittsburghMinersTeamAttributeDictionary
        {
            get
            {
                if (_PittsburghMinersTeamAttributeDictionary == null)
                {
                    _PittsburghMinersTeamAttributeDictionary = new Dictionary<string, int>();
                    _PittsburghMinersTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _PittsburghMinersTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _PittsburghMinersTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _PittsburghMinersTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _PittsburghMinersTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> CincinnatiTigersTeamAttributeDictionary
        {
            get
            {
                if (_CincinnatiTigersTeamAttributeDictionary == null)
                {
                    _CincinnatiTigersTeamAttributeDictionary = new Dictionary<string, int>();
                    _CincinnatiTigersTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _CincinnatiTigersTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _CincinnatiTigersTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _CincinnatiTigersTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _CincinnatiTigersTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> BaltimoreVulturesTeamAttributeDictionary
        {
            get
            {
                if (_BaltimoreVulturesTeamAttributeDictionary == null)
                {
                    _BaltimoreVulturesTeamAttributeDictionary = new Dictionary<string, int>();
                    _BaltimoreVulturesTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _BaltimoreVulturesTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _BaltimoreVulturesTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _BaltimoreVulturesTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _BaltimoreVulturesTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> ClevelandBluesTeamAttributeDictionary
        {
            get
            {
                if (_ClevelandBluesTeamAttributeDictionary == null)
                {
                    _ClevelandBluesTeamAttributeDictionary = new Dictionary<string, int>();
                    _ClevelandBluesTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _ClevelandBluesTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _ClevelandBluesTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _ClevelandBluesTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _ClevelandBluesTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> KansasCityNativesTeamAttributeDictionary
        {
            get
            {
                if (_KansasCityNativesTeamAttributeDictionary == null)
                {
                    _KansasCityNativesTeamAttributeDictionary = new Dictionary<string, int>();
                    _KansasCityNativesTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _KansasCityNativesTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _KansasCityNativesTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _KansasCityNativesTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _KansasCityNativesTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> OaklandPillagersTeamAttributeDictionary
        {
            get
            {
                if (_OaklandPillagersTeamAttributeDictionary == null)
                {
                    _OaklandPillagersTeamAttributeDictionary = new Dictionary<string, int>();
                    _OaklandPillagersTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _OaklandPillagersTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _OaklandPillagersTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _OaklandPillagersTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _OaklandPillagersTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> SanDiegoPowerTeamAttributeDictionary
        {
            get
            {
                if (_SanDiegoPowerTeamAttributeDictionary == null)
                {
                    _SanDiegoPowerTeamAttributeDictionary = new Dictionary<string, int>();
                    _SanDiegoPowerTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _SanDiegoPowerTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _SanDiegoPowerTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _SanDiegoPowerTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _SanDiegoPowerTeamAttributeDictionary;
            }
        }

        public static Dictionary<string, int> DenverMustangsTeamAttributeDictionary
        {
            get
            {
                if (_DenverMustangsTeamAttributeDictionary == null)
                {
                    _DenverMustangsTeamAttributeDictionary = new Dictionary<string, int>();
                    _DenverMustangsTeamAttributeDictionary.Add(FanLoyalty, 9);
                    _DenverMustangsTeamAttributeDictionary.Add(MegaBowlsWon, 3);
                    _DenverMustangsTeamAttributeDictionary.Add(HallOfFamers, 10);
                    _DenverMustangsTeamAttributeDictionary.Add(Prestige, 8);
                }
                return _DenverMustangsTeamAttributeDictionary;
            }
        }                   

        public static List<string> TeamList
        {
            get
            {
                if (_TeamList == null)
                {
                    _TeamList = new List<string>();

                    _TeamList.Add(NewYorkTitans);
                    _TeamList.Add(PhiledelphiaTurkeys);
                    _TeamList.Add(WashingtonMemorials);
                    _TeamList.Add(DallasAmericans);

                    _TeamList.Add(NewOrleansCajuns);
                    _TeamList.Add(AtlantaFowls);
                    _TeamList.Add(CarolinaCats);
                    _TeamList.Add(TampaBayPirates);

                    _TeamList.Add(ChicagoGrizzlies);
                    _TeamList.Add(GreenBayBackers);
                    _TeamList.Add(DetroitCheetahs);
                    _TeamList.Add(MinnesotaCrusaders);

                    _TeamList.Add(Sanfrancisco1999ers);
                    _TeamList.Add(StLouisGoats);
                    _TeamList.Add(SeattleSeagulls);
                    _TeamList.Add(ArizonaRedBirds);

                    _TeamList.Add(NewJerseyMigs);
                    _TeamList.Add(NewEnglandTeaBags);
                    _TeamList.Add(MiamiSharks);
                    _TeamList.Add(Buffalos);

                    _TeamList.Add(TennesseMountains);
                    _TeamList.Add(IndianapolisThoroughbreds);
                    _TeamList.Add(HoustonAstronauts);
                    _TeamList.Add(JacksonvilleLynx);
                    
                    _TeamList.Add(PittsburghMiners);
                    _TeamList.Add(CincinnatiTigers);
                    _TeamList.Add(BaltimoreVultures);
                    _TeamList.Add(ClevelandBlues);
                    
                    _TeamList.Add(KansasCityNatives);
                    _TeamList.Add(OaklandPillagers);
                    _TeamList.Add(SanDiegoPower);
                    _TeamList.Add(DenverMustangs);

                }
                return _TeamList;
            }
        }
        #endregion
    }
}
