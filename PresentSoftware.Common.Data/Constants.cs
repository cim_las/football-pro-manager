﻿namespace PresentSoftware.Common.Data
{
    public static class Constants
    {
        public const int NUMBEROFROOKIES = 2500;
        public const int SHOWMESSAGEBOXES = 1;
        public const int ZERO = 0;
        public const int FIFTEEN = 15;
        public const int FIFTY = 50;
        public const int BUTTONGRIDCOLUMNS = 17;
        public const int BUTTONGRIDROWS = 11;
        public const int MAXNUMBERPLAYS = 10;
        public const int MINNUMBERPLAYS = 0;
        public const int MAXNUMBERFORMATIONS = 15;
        public const int MINNUMBERFORMATIONS = 0;
        public const int DEPTHCHARTSIZE = 15;
        public const int NUMBEROFJERSEYNUMBERS = 100;

        public const int DaysInYear = 365;

        //a few useful constants
        public const int MAXINT = 7777777;
        public const double MAXDOUBLE = 7777777.77;
        public const double MINDOUBLE = 0.0;

        public const double PI = 3.14159;
        public const double TWOPI = PI * 2;
        public const double HALFPI = PI / 2;
        public const double QUARTERPI = PI / 4;

        
        public static int NumberOfLeagues = 1;
        public static int NumberOfConferences = 2;
        public static int NumberOfDivisions = 4;
        public static int NumberOfTeamsPerDivision = 4;
        
        public const int INT_MaxAbility = 1000;

        public const int INT_SevenStar = 900;
        public const int INT_SixStar = 750;
        public const int INT_FiveStar = 600;
        public const int INT_FourStar = 450;
        public const int INT_ThreeStar = 300;
        public const int INT_TwoStar = 150;
        public const int INT_OneStar = 1;
        public const string STR_UnknownStar = "?";

        public const int INT_NonFocusedFactor = 10;
        public const int INT_FocusedFactor = 2;

        public const int INT_MaxCategories = 5;
    }
}
