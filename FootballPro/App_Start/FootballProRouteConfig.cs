using System.Web.Mvc;

[assembly: WebActivator.PreApplicationStartMethod(
    typeof(FootballPro.App_Start.FootballProRouteConfig), "RegisterFootballProPreStart", Order = 2)]

namespace FootballPro.App_Start
{
    ///<summary>
    /// Inserts the Football Pro controller to the front of all MVC routes
    /// so that the Football Pro becomes the default page.
    ///</summary>
    ///<remarks>
    /// This class is discovered and run during startup
    /// http://blogs.msdn.com/b/davidebb/archive/2010/10/11/light-up-your-nupacks-with-startup-code-and-webactivator.aspx
    ///</remarks>
    public static class FootballProRouteConfig
    {

        public static void RegisterFootballProPreStart()
        {

            // Preempt standard default MVC page routing to go to Football Pro
            System.Web.Routing.RouteTable.Routes.MapRoute(
                name: "FootballProMvc",
                url: "{controller}/{action}/{id}",
                defaults: new
                {
                    controller = "FootballPro",
                    action = "Index",
                    id = UrlParameter.Optional
                }
                );
        }
    }
}