using System;
using System.Web.Optimization;

[assembly: WebActivator.PostApplicationStartMethod(
    typeof(FootballPro.App_Start.FootballProConfig), "PreStart")]

namespace FootballPro.App_Start
{
    public static class FootballProConfig
    {
        public static void PreStart()
        {
            // Add your start logic here
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}