using System.Linq;
using System.Web.Http;
using Breeze.WebApi;
using FootballPro.Models;
using Newtonsoft.Json.Linq;

namespace FootballPro.Controllers
{
    [BreezeController]
    public class BreezeController : ApiController
    {
        readonly EFContextProvider<FootballProDbContext> _contextProvider =
        new EFContextProvider<FootballProDbContext>();

        // ~/breeze/persons/Metadata
        [HttpGet]
        public string Metadata()
        {
            return _contextProvider.Metadata();
        }

        // ~/breeze/persons/SaveChanges
        [HttpPost]
        public SaveResult SaveChanges(JObject pSaveBundle)
        {
            return _contextProvider.SaveChanges(pSaveBundle);
        }

        // ~/breeze/persons/Persons
        // ~/breeze/persons/Persons?$filter=IsArchived eq false&$orderby=CreatedAt
        [HttpGet]
        public IQueryable<Person> Persons()
        {
            return _contextProvider.Context.Persons;
        }
    }
}
