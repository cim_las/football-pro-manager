﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootballPro.Models
{
    public class Person
    {
        public int Id { get; set; }
        //Bio
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int HeightInInches { get; set; }
        public int WeightInPounds { get; set; }

        //Mental
        public int Aggresiveness { get; set; }
        public int Bravery { get; set; }
        public int Creativity { get; set; }
        public int DecisionMaking { get; set; }
        public int Determination { get; set; }
        public int Honesty { get; set; }
        public int Leadership { get; set; }
        public int Selfishness { get; set; }
        public int Toughness { get; set; }
        public int Showmanship { get; set; }
        public int Intelligence { get; set; }
        public int Wisdom { get; set; }
        public int Faith { get; set; }
        public int Discipline { get; set; }
        public int Seriousness { get; set; }
        public int Humor { get; set; }
        public int Empathy { get; set; }

        public int Position { get; set; }
        public int Speed { get; set; }
        public int Power { get; set; }
        public int DeepThrows { get; set; }
        public int MediumThrows { get; set; }
        public int ShortThrows { get; set; }
        public int Awareness { get; set; }
        public int Catching { get; set; }
        public int Blocking { get; set; }
        public int Tackling { get; set; }
        public int Covering { get; set; }
        public int RouteRunning { get; set; }
        public int SpecialTeams { get; set; }
        public int Kicking { get; set; }
        public int Punting { get; set; }
        public int AbilityCap { get; set; }


        
        public string Job { get; set; }
        
        
        public string College { get; set; }

        
        
        public bool IsScouted { get; set; }
        public bool IsFreeAgent { get; set; }
        public string ScoutedBy { get; set; }
        public int DraftPickNumber { get; set; }

        public int Ability { get; set; }

        

        public int PassDeep { get; set; }
        public int PassMedium { get; set; }
        public int PassShort { get; set; }
        public int PocketPresence { get; set; }
        
        public int Hands { get; set; }
        public int Technique { get; set; }
        public int Vision { get; set; }
        
        public int ManCoverage { get; set; }
        public int ZoneCoverage { get; set; }
        public int PassRush { get; set; }
        public int RunStopping { get; set; }
        public int PassBlocking { get; set; }
        public int RunBlocking { get; set; }
        
        public int Returning { get; set; }
        public int Acceleration { get; set; }
        public int Agility { get; set; }
        public int Leaping { get; set; }

        public int Stamina { get; set; }
        public int Balance { get; set; }
        public int Durability { get; set; }
        public int Size { get; set; }
        
        
        
        public int Motor { get; set; }
        public int Recognition { get; set; }
        public int CoachOffense { get; set; }
        public int CoachDefense { get; set; }
        public int CoachSpecialTeams { get; set; }
        public int PlayerEval { get; set; }

        
        
        public int StarBowls { get; set; }
    }
}