﻿define(['durandal/app', 'services/datacontext', 'plugins/router', 'services/logger'],
    function(app, datacontext, router, logger)
    {
        var title = 'Create Owner';
        var owner = ko.observable();
        var isSaving = ko.observable(false);
        var cancel = function(complete)
        {
            router.navigateBack();
        };
        var hasChanges = ko.computed(function()
        {
            return datacontext.hasChanges();
        });
        var canSave = ko.computed(function()
        {
            return hasChanges() && !isSaving();
        });
        var activate = function()
        {
            logger.log(title + ' View Activated', null, title, true);
            initLookups();
            owner(datacontext.createOwner());
            return true;
        };
        var initLookups = function()
        {
            //rooms(datacontext.lookups.rooms);
            //timeSlots(datacontext.lookups.timeslots);
            //tracks(datacontext.lookups.tracks);
            //speakers(datacontext.lookups.speakers);
        };
        var save = function()
        {
            isSaving(true);
            datacontext.saveChanges()
                .then(goToEditView).fin(complete);

            function goToEditView(result)
            {
                router.replaceLocation('#/ownerdetail/' + owner().id());
            }

            function complete()
            {
                isSaving(false);
            }
        };
        var canDeactivate = function()
        {
            if (hasChanges())
            {
                var msg = 'Do you want to leave and cancel?';
                return app.showMessage(msg, 'Navigate Away', ['Yes', 'No'])
                    .then(function(selectedOption)
                    {
                        if (selectedOption === 'Yes')
                        {
                            datacontext.cancelChanges();
                        }
                        return selectedOption;
                    });
            }
            return true;
        };

        var vm =
        {
            activate: activate,
            title: title,
            canDeactivate: canDeactivate,
            canSave: canSave,
            cancel: cancel,
            hasChanges: hasChanges,
            save: save,
            owner: owner,
        };

        return vm;

        //#region Internal Methods
        //function activate()
        //{
        //    logger.log(title + ' View Activated', null, title, true);
        //    initLookups();
        //    owner(datacontext.createOwner());
        //    return true;
        //}

//#endregion
    });