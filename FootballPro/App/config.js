﻿define(function()
{
    toastr.options.timeOut = 4000;
    toastr.options.positionClass = 'toast-bottom-right';

    var imageSettings = {
        imageBasePath: '../content/images/photos/',
        unknownPersonImageSource: 'unknown_person.jpg'
    };

    var remoteServiceName = 'breeze/Breeze';

    var appTitle = 'Football Pro';

    var routes = [
        //{
        //    url: 'sessions',
        //    moduleId: 'viewmodels/sessions',
        //    name: 'Sessions',
        //    visible: true,
        //    caption: 'Sessions',
        //    settings: { caption: '<i class="icon-book"></i> Sessions' }
        //}, {
        //    url: 'speakers',
        //    moduleId: 'viewmodels/speakers',
        //    name: 'Speakers',
        //    caption: 'Speakers',
        //    visible: true,
        //    settings: { caption: '<i class="icon-user"></i> Speakers' }
        //}, {
        //    url: 'sessiondetail/:id',
        //    moduleId: 'viewmodels/sessiondetail',
        //    name: 'Edit Session',
        //    caption: 'Edit Session',
        //    visible: false
        //}, 
        { route: '', moduleId: 'home', title: 'Home', nav: 1 },
        { route: 'details', moduleId: 'details', title: 'Details', nav: 2 },
        { route: 'createOwner', moduleId: 'createOwner', title: 'Create Owner', visible: true, nav: 3 }
        //{
        //    url: 'createOwner',
        //    moduleId: 'viewmodels/createOwner',
        //    name: 'Create Owner',
        //    visible: true,
        //    caption: 'Create Owner',
        //    settings: { caption: '<i class="icon-user"></i> Owner' }
        //}
    ];

    var startModule = '';

    return {
        appTitle: appTitle,
        debugEnabled: ko.observable(true),
        imageSettings: imageSettings,
        remoteServiceName: remoteServiceName,
        routes: routes,
        startModule: startModule
    };
});